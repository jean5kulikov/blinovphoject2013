package variantA.task3;

import variantA.task1.ConsoleInput;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Gene
 * Date: 22.04.15
 * Time: 22:33
 * To change this template use File | Settings | File Templates.
 */
public class RandomNumbersShower {
    private ConsoleInput consoleInput;

    public RandomNumbersShower() {
        consoleInput = new ConsoleInput();
    }

    public int typeQuantityRandomNumber() {
        int quantity = Integer.valueOf(consoleInput.getInputtedValue());
        if (quantity <= 0) {
            typeQuantityRandomNumber();
        }
        return quantity;
    }

    public int typeMaxValue() {
        int maxValue = Integer.valueOf(consoleInput.getInputtedValue());
        if (maxValue <= 0) {
            typeMaxValue();
        }
        return maxValue;
    }

    public int[] getRandomArray(int maxValue, int quantityOnNumbers) {
        int[] resultArray = new int[quantityOnNumbers];
        Random generator;

        generator = new Random();
        for (int i = 0; i < quantityOnNumbers; i++) {
            resultArray[i] = generator.nextInt(maxValue);
        }
        return resultArray;
    }

    public static void main(String[] args) {
        RandomNumbersShower randomNumbersShower = new RandomNumbersShower();
        int quantity = randomNumbersShower.typeQuantityRandomNumber();
        int maxNumber = randomNumbersShower.typeMaxValue();
        int[] array = randomNumbersShower.getRandomArray(maxNumber, quantity);
        for (int i : array) {
            System.out.println(i);
        }
        for (int i : array) {
            System.out.printf("%s ", i);
        }
    }
}

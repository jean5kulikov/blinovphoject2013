package variantA.task4;

import variantA.task1.ConsoleInput;

import java.io.Console;

/**
 * Created with IntelliJ IDEA.
 * User: Gene
 * Date: 22.04.15
 * Time: 22:57
 * To change this template use File | Settings | File Templates.
 */
public class CheckPassword {

    private ConsoleInput consoleInput;
    private String originalPassword;

    public CheckPassword() {
        consoleInput = new ConsoleInput();

    }

    public String typePassword() {
         return consoleInput.getInputtedValue();
    }

    public boolean comparePasswords(String originalPassword, String inputtedPassword) {
        return originalPassword.equals(inputtedPassword);
    }

    public static void main(String[] args) {
        CheckPassword checkPassword = new CheckPassword();
        String originalPassword =  checkPassword.typePassword();
        String password =  checkPassword.typePassword();
        System.out.println(checkPassword.comparePasswords(originalPassword, password));
    }
}





package variantA.task1;

/**
 * Created with IntelliJ IDEA.
 * User: Gene
 * Date: 22.04.15
 * Time: 22:10
 * To change this template use File | Settings | File Templates.
 */
public class NameAsker {

    private ConsoleInput consoleInput;

    public NameAsker(){
        consoleInput = new ConsoleInput();
    }

    public String startAskName(){
        return consoleInput.getInputtedValue();
    }

    public void welcomeUser(String user){
        System.out.println(String.format("Hello %s", user));
    }

    public static void main(String [] args){
        NameAsker nameAsker = new NameAsker();
        String userNameVariable = null;

        while(true){
            userNameVariable = nameAsker.startAskName();
            if(userNameVariable.equals("c")){
                break;
            }
            else{
                nameAsker.welcomeUser(userNameVariable);
            }
        }
    }
}

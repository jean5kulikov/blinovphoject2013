package variantA.task1;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Gene
 * Date: 22.04.15
 * Time: 22:05
 * To change this template use File | Settings | File Templates.
 */
public class ConsoleInput {

    private Scanner scanner;

    public ConsoleInput() {
        scanner = new Scanner(System.in);
    }

    public String getInputtedValue(){
        return scanner.next();
    }
}

package variantA.task5;

/**
 * Created with IntelliJ IDEA.
 * User: Gene
 * Date: 22.04.15
 * Time: 23:27
 * To change this template use File | Settings | File Templates.
 */
public class ArgumentsManipulator {
    public static void main(String[] args) {
        int sum = 0, plural = 1, tmp;
        for (String oneArg : args) {
            tmp = Integer.valueOf(oneArg);
            sum += tmp;
        }
        for (String oneArg : args) {
            tmp = Integer.valueOf(oneArg);
            plural *= tmp;
        }
        System.out.printf("Summa %s , Plural %s", sum, plural);
    }
}

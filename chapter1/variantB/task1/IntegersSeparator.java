package variantB.task1;

import variantA.task1.ConsoleInput;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gene on 28.04.2015.
 */
public class IntegersSeparator {

    private ConsoleInput consoleInput;
    private int numbersQuantity;

    public IntegersSeparator() {
        consoleInput = new ConsoleInput();
    }

    public int enterNumbersQuantity() {
        return Integer.valueOf(consoleInput.getInputtedValue());
    }

    public int[] fillArray(int numbersQuantity) {
        int[] numbersArray = new int[numbersQuantity];
        for (int i = 0; i < numbersArray.length; i++) {
            numbersArray[i] = enterNumbersQuantity();
        }
        return numbersArray;
    }

    //------    task 1
    public List<Integer> getEvenOrOddDigitsArray(String numbersType, int[] array) {
        ArrayList<Integer> evenDigits = new ArrayList<Integer>();
        ArrayList<Integer> oddDigits = new ArrayList<Integer>();

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                evenDigits.add(array[i]);
            } else {
                oddDigits.add(array[i]);
            }
        }
        if (numbersType.equals("even")) {
            return evenDigits;
        } else if (numbersType.equals("odd")) {
            return oddDigits;
        } else {
            throw new IllegalArgumentException(String.format("%s - unsupported format", numbersType));
        }
    }
    //------    task 2

    public int getMaxInt(int[] array) {
        int maxInt = array[0];
        for (int i = 0; i < array.length; i++) {
            if (maxInt < array[i]) {
                maxInt = array[i];
            }
        }
        return maxInt;
    }

    public int getMinInt(int[] array) {
        int minInt = array[0];
        for (int i = 0; i < array.length; i++) {
            if (minInt > array[i]) {
                minInt = array[i];
            }
        }
        return minInt;
    }

    //------    task 3


    public List<Integer> getIntegersDividedOnThreeOrNine(int[] array) {
        List<Integer> resultList = new ArrayList<Integer>();
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 3 == 0 | array[i] % 9 == 0) {
                resultList.add(array[i]);
            }
        }
        return resultList;
    }

    //------    task 4

    public List<Integer> getIntegersDividedOnFiveAndSeven(int[] array) {
        List<Integer> resultList = new ArrayList<Integer>();
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 5 == 0 & array[i] % 7 == 0) {
                resultList.add(array[i]);
            }
        }
        return resultList;
    }

    //------    task 5

    public int[] getArrayModuleMinByBubble(int[] array) {
        int[] modulesArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            modulesArray[i] = Math.abs(array[i]);
        }

        for (int i = 0; i < modulesArray.length - 1; i++) {
            for (int j = 0; j < modulesArray.length - i - 1; j++) {
                if (modulesArray[j] < modulesArray[j + 1]) {
                    int tmp = modulesArray[j];
                    modulesArray[j] = modulesArray[j + 1];
                    modulesArray[j + 1] = tmp;
                }
            }
        }

        return modulesArray;
    }

    public void showArrayOnConsole(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    //------    task 6

    public List<Integer> getListWithThreeDigitsWithoutRepeatingDigits(int[] array) {
        List<Integer> resultList = new ArrayList<Integer>();
        for (int i = 0; i < array.length; i++) {
            if (isThreeDigitInteger(array[i]) & isSingleCharacters(String.valueOf(array[i]))) {
                resultList.add(array[i]);
            }
        }
        return resultList;
    }

    public boolean isThreeDigitInteger(int integer) {
        if (String.valueOf(integer).length() == 3) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isSingleCharacters(String digitAsString) {
        String tempString = digitAsString + ".";
        char[] charSequence = tempString.toCharArray();
        boolean flag = false;
        for (int i = 0; i < charSequence.length; i++) {

            String[] arrayAfterSplit = tempString.split(String.valueOf(charSequence[i]));

            if (arrayAfterSplit.length > 2) {
                flag = false;
                break;
            } else {
                flag = true;
                continue;
            }
        }
        return flag;
    }

    //------    task 7

    public List<Integer> getMaxCommonDivider(int[] array) {
        List<Integer> listOfNodes = new ArrayList<Integer>();


        int maxInteger = getMaxInt(array);



        for (int i = 0; i < array.length; i++) {
            System.out.println("value:  " + array[i]);


            for(int j=1; j<maxInteger; j++){
                listOfNodes.add(j);
            }

            System.out.println();
        }


        return listOfNodes;
    }


//    public int getNOD(List<List<Integer>> lists){
//        I
//    }


    public int[] getFilledArrayWithNumber(int firstInteger, int lastInteger) {
        if (firstInteger > lastInteger) {
            throw new IllegalArgumentException("firstInteger more then lastInteger");
        } else {
            int[] array = new int[lastInteger - firstInteger];
            for (int i = 0; i < lastInteger - firstInteger; i++) {
                array[i] = firstInteger + i;
            }
            return array;
        }
    }


    public static void main(String[] args) {
        IntegersSeparator integersSeparator = new IntegersSeparator();
        int numbersQuantity = integersSeparator.enterNumbersQuantity();
        int[] array = integersSeparator.fillArray(numbersQuantity);
//        System.out.println("Max: " + integersSeparator.getMaxInt(array));
//        System.out.println("Min: " + integersSeparator.getMinInt(array));
//        System.out.println("Divided On 3 Or 9: " + integersSeparator.getIntegersDividedOnThreeOrNine(array));
//        System.out.println("Divided On 5 And 7: " + integersSeparator.getIntegersDividedOnFiveAndSeven(array));
//        System.out.println("Decrease modules array sorted by bubble: " );
//        integersSeparator.showArrayOnConsole(integersSeparator.getArrayModuleMinByBubble(array));//
//        int [] array = integersSeparator.getFilledArrayWithNumber(500, 1000);
//        System.out.println(integersSeparator.getListWithThreeDigitsWithoutRepeatingDigits(array));


//        integersSeparator.getMaxCommonDivider(array);

        System.out.println(integersSeparator.getMaxCommonDivider(array));


    }

}
